package de.ppp2016;

import de.ppp2016.Parser.GelbeListe;
import de.ppp2016.Parser.GelbeListe2.GelbeListe2;
import de.ppp2016.Parser.RoteListe.RotParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by jjensen on 4/13/16.
 */
public class Benchmark {
    private int repeats, maxThreads;
    private Database db;
    private Path dbpath, outputfile;
    private String dbUrl, dbuser, dbpassword;

    Benchmark(Map<String, String> config) {
        String s;
        s = config.get("outputfile");
        if (s != null) this.outputfile = Paths.get(s);
        else this.outputfile = Paths.get("output.txt");
        s = config.get("repeats");
        if (s != null) this.repeats = Integer.parseInt(s);
        else this.repeats = 1;
        s = config.get("max_threads");
        if (s != null) this.maxThreads = Integer.parseInt(s);
        else this.maxThreads = 1;
        s = config.get("db_path");
        if (s != null) this.dbpath = Paths.get(s);
        s = config.get("db");
        if (s != null) this.db = Database.valueOf(s);
        this.dbUrl = config.get("db_url");
        this.dbuser = config.get("user");
        this.dbpassword = config.get("password");
    }

    private boolean checkConfig() {
        if (db == null) {
            System.err.println("Database not specified");
            return false;
        }
        if (dbUrl == null) {
            System.err.println("Database URL not specified");
            return false;
        }
        return true;
    }

    void start() {
        if (!checkConfig()) return;
        //warmup
        runParser(1);
        runParser(1);

        //benchmark
        try {
            FileWriter writer = new FileWriter(outputfile.toFile());
            for (int i = 1; i <= maxThreads; i++) {
                long time = 0;
                for (int j = 0; j < repeats; j++) {
                    time += runParser(i);
                }
                System.out.println(i+","+ time / repeats);
                writer.write(i+","+ time / repeats + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private long runParser(int i) {
        switch (db) {
            case GELBE:
                GelbeListe liste = new GelbeListe(dbpath, dbUrl, dbuser, dbpassword, i, null);
                liste.run();
                return liste.getProgress().time();
            case GELBE2:
                try {
                    GelbeListe2 liste2 = new GelbeListe2(dbpath, dbUrl, dbuser, dbpassword, i, null);
                    liste2.run();
                    return liste2.getProgress().time();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.exit(12);
            case ROTE:
                try {
                    RotParser rotParser = new RotParser(null, dbpath.toString(), dbUrl, dbuser, dbpassword, i);
                    rotParser.run();
                    return rotParser.getProgress().time();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
                System.exit(12);
        }
        return 0;
    }
}
