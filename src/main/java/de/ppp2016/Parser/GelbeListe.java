package de.ppp2016.Parser;

import com.opencsv.CSVReader;
import de.ppp2016.Progress;
import javafx.scene.control.Button;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by jjensen on 3/3/16.
 */
public class GelbeListe extends Thread {

    private final static boolean FKHACK = true;
    private final static int PARALLEL_ROW_MAX = 100000;
    private final int threads;
    private final Path databaseDirectory;
    private final String dbURL, user, pass;
    private final Button startButton;
    private Progress progress;

    public GelbeListe(Path databaseDirectory, String dbURL, String user, String pass, int threads, Button startButton) {
        this.startButton = startButton;
        this.threads = threads;
        this.databaseDirectory = databaseDirectory;
        this.dbURL = dbURL;
        this.user = user;
        this.pass = pass;
        progress = new Progress();
    }

    @Override
    public synchronized void run() {
        try {
            try {
                Statement stmnt = DriverManager.getConnection(dbURL, user, pass).createStatement();
                stmnt.execute("SET FOREIGN_KEY_CHECKS=0");
                System.out.println("Recreating Database");
                boolean successful = !Files.lines(new File("res/gelbeListeCreate.sql").toPath()).map((s) -> {
                    if (s.isEmpty()) return true;
                    try {
                        stmnt.execute(s);
                        return true;
                    } catch (SQLException e) {
                        System.err.println("Could not recreate database: " + e.getMessage());
                    }
                    return false;
                }).allMatch(Boolean::booleanValue);
                stmnt.execute("SET FOREIGN_KEY_CHECKS=1");
                if (successful) System.exit(-1);
                System.out.println("Recreation finished");
            } catch (SQLException e) {
                e.printStackTrace();
            }

            long size = 0;
            for (Path path : Files.newDirectoryStream(databaseDirectory, "*CSV")) {
                size += Files.lines(path).count() - 1;
            }
            progress.setSize(size);

            //copy all tables
            System.out.println("Running with " + threads + " thread(s)");
            int processorNumb = Runtime.getRuntime().availableProcessors();
            if (processorNumb < threads) {
                System.out.println("But only " + processorNumb + " processors were found");
            }
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(this.threads);
            for (Path path : Files.newDirectoryStream(databaseDirectory, "*CSV")) {
                executor.execute(() -> copyTable(path, dbURL, user, pass));
            }

            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
            if (this.startButton!=null) this.startButton.setDisable(false);
            System.out.println("Finish");
            System.out.println(progress.time());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void copyTable(Path tablePath, String dbURL, String user, String pass) {
        String fileName = tablePath.getFileName().toString().split("\\.")[0];
        String tableName = "gliste_" + fileName;
        try {
            CSVReader tableReader = new CSVReader(new FileReader(tablePath.toString()), ';');

            //create querrie for prepared stmnt
            String[] line = tableReader.readNext();
            if (line.length < 1) return;
            StringBuilder columNamesBuilder = new StringBuilder("`").append(line[0]);
            for (int i = 1; i < line.length; i++) {
                String s = line[i];
                columNamesBuilder.append("`,`").append(s);
            }
            columNamesBuilder.append("`");
            String columnNames = columNamesBuilder.toString();
            StringBuilder prepQuerry = new StringBuilder("INSERT INTO ").append(tableName).append(" (").append(columnNames)
                    .append(") VALUES (").append(columnNames.replaceAll("[^,]+", "?")).append(")");

            int count = 0;
            try {
                PreparedStatement prepStmnt = DriverManager.getConnection(dbURL + "?rewriteBatchedStatements=true", user, pass).prepareStatement(prepQuerry.toString());
                if (FKHACK) {
                    prepStmnt.execute("SET FOREIGN_KEY_CHECKS=0");
                }

                //copy all rows
                while ((line = tableReader.readNext()) != null) {
                    if (count >= PARALLEL_ROW_MAX) {
                        try {
                            prepStmnt.executeBatch();
                        } catch (SQLException e) {
                            System.err.println(e.getMessage());
                        }
                        progress.inc(count);
                        count = 0;
                    }
                    count++;
                    copyRow(line, prepStmnt);
                }
                prepStmnt.executeBatch();
                progress.inc(count);

                if (FKHACK) {
                    prepStmnt.execute("SET FOREIGN_KEY_CHECKS=1");
                }
                prepStmnt.close();
            } catch (SQLException e) {
                System.err.print("In tabe " + tableName + ": ");
                System.err.println(e.getMessage());
            }
            //progress.inc(count);
            tableReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void copyRow(String[] row, PreparedStatement prepStmnt) {
        try {
            for (int i = 0; i < row.length; i++) {
                String s = row[i];
                s = s.replace("\"", "");
                //if its empty
                if (s.isEmpty()) {
                    prepStmnt.setNull(i + 1, Types.OTHER);
                } else {
                    //if its a date
                    if (s.length() == 10) {
                        try {
                            Date date = new SimpleDateFormat("dd.MM.yyyy").parse(s);
                            prepStmnt.setString(i + 1, new SimpleDateFormat("yyyy-MM-dd").format(date));
                            continue;
                        } catch (ParseException ignored) {
                        }
                    }
                    s = s.replace(",", ".");
                    prepStmnt.setString(i + 1, s);
                }
            }
            prepStmnt.addBatch();
        } catch (SQLException e) {
            System.out.println(Arrays.toString(row));
            System.out.println(prepStmnt);
            e.printStackTrace();
        }
    }

    public Progress getProgress() {
        return progress;
    }
}
