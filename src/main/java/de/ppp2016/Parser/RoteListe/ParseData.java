package de.ppp2016.Parser.RoteListe;

import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by awalende on 28.02.16.
 * Parsing and inserting data into mysql.
 */
public class ParseData {

    private Connection conn;
    private Table table;
    private final String tableName;

    public ParseData(Connection conn, Table table) {
        this.conn = conn;
        this.table = table;
        this.tableName = table.getName();
    }

    //Creates the insert statements for mysql and parses them.
    public void fillTable() {
        //Build the INSERT String
        String query_temp = "INSERT INTO " + "rliste_" +  tableName + " (";
        String query = query_temp;
        for (int x = 0; x < table.getColumnCount(); x++) {
            query = query + table.getColumns().get(x).getName() + ", ";
        }
        query = query.substring(0, query.length() - 2);
        query = query + ") VALUES (";
        for (int x = 0; x < table.getColumnCount(); x++) {
            query = query + "?, ";
        }
        query = query.substring(0, query.length() - 2);
        query = query + ")";

        List<PreparedStatement> statementList = new LinkedList<PreparedStatement>();
        //Fill Data in statement (maybe needs to be recreated all the time?
        try {
            //Use prepared statements for sql injection safety.
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            for (Row row : table) {

                for (int x = 0; x < table.getColumnCount(); x++) {
                    if (row.get(table.getColumns().get(x).getName()) != null) {
                        preparedStatement.setString(x + 1, row.get(table.getColumns().get(x).getName()).toString());
                    } else {
                        preparedStatement.setString(x + 1, null);
                    }
                }
                //Use batches and commit them at once, for better speedup.
                preparedStatement.addBatch();
            }
            //For debugging?
            int[] status = preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
