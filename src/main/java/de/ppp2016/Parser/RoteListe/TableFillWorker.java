package de.ppp2016.Parser.RoteListe;

import com.healthmarketscience.jackcess.Table;
import de.ppp2016.Progress;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by awalende on 14.03.16.
 * Workerclass for inserting and parsing data to mysql.
 * Work is divided in Tables available.
 */
public class TableFillWorker implements Runnable {

    private Table table;
    private final String db_url;
    private final String user;
    private final String pass;
    private Progress progress;

    public TableFillWorker(Table table, Progress progress, String DB_URL, String USER, String PASS) {
        this.table = table;
        db_url = DB_URL;
        user = USER;
        pass = PASS;
        this.progress = progress;
    }

    public TableFillWorker(Table table, String DB_URL, String USER, String PASS) {
        this.table = table;
        db_url = DB_URL;
        user = USER;
        pass = PASS;
    }


    //Setup a new mysql connection for better multi-thread performance.
    @Override
    public void run() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //Connection conn = DriverManager.getConnection(db_url, user, pass);
            Connection conn = DriverManager.getConnection(db_url, user, pass);
            ParseData parseData = new ParseData(conn, table);
            parseData.fillTable();
            System.out.println("Table filled: " + this.table.getName());

            if(progress != null)
                progress.inc(1);
            conn.close();


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
