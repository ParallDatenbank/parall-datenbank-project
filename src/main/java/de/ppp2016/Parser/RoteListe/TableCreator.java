package de.ppp2016.Parser.RoteListe;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by awalende on 27.02.16.
 * Creates mysql schema, primary keys and foreign keys.
 * Creation plan is located in the /res folder.
 */
public class TableCreator {


    private List<String> createList;
    private List<String> foreignList;

    public TableCreator() {
        fillList();
    }


    //Read the mysql instruction files.
    public void fillList(){
        String query = "";
        createList = new ArrayList<String>();
        createList = createSchemaTable("res/schema.sql");

        foreignList = new ArrayList<String>();
        foreignList = createSchemaTable("res/foreignkey.sql");
    }


    //Convert lines into a list.
    private List<String> createSchemaTable(String path){
        FileReader fileReader = null;
        List<String> schemas = new ArrayList<String>();
        try {
            fileReader = new FileReader(path);
        } catch (FileNotFoundException e) {
            System.out.println("Can't find " + path);
            System.exit(-1);
            e.printStackTrace();
        }

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        String query = "";
        try {
            while ((line = bufferedReader.readLine()) != null){
                    query = query.concat(line);
                    if(line.contains(";")){
                        schemas.add(query);
                        query = "";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return schemas;
    }


    public List<String> getCreateList() {
        return createList;
    }

    public List<String> getForeignList() {
        return foreignList;
    }

}
