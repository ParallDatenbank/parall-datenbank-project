package de.ppp2016.Parser.RoteListe;

import com.healthmarketscience.jackcess.*;
import de.ppp2016.Progress;
import de.ppp2016.MainWindow;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by awale on 24.02.2016.
 */

    /*
    Class responsible for parsing ms-access file to MySQL.
     */
public class RotParser extends Thread {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private String FILE_PATH = null;
    private String DB_URL = null;
    private String USER = null;
    private String PASS = null;
    private int THREADCOUNT = 0;
    private MainWindow mainWindow;
    private Progress progress;



    //Basic constructor for all the basic needs in human life.
    public RotParser(MainWindow mainWindow, String filePath, String dbUrl, String user, String pass, int threadCount) throws IOException, SQLException {
        this.mainWindow = mainWindow;
        this.progress = new Progress();
        DB_URL = dbUrl + "?rewriteBatchedStatements=true";
        USER = user;
        PASS = pass;
        THREADCOUNT = threadCount;
        FILE_PATH = filePath;
    }


    /*
    Single thread for running the show, contains ability to create more Threads with the
    ExecutorService. Don't know if synchronized is actually useful, simply copied it from joris.
     */
    @Override
    public synchronized void run(){
        Connection conn = null;
        Statement stmt = null;

        //Object for creating the table-schema with keys.
        TableCreator tc = new TableCreator();

        //Start recording time from here. It measures dropping, creating, setting keys and filling tables.
        long beginningTime = System.currentTimeMillis();
        try {
            //Register mysql-connector
            Class.forName("com.mysql.jdbc.Driver");

            //Using jackcess API for extracting data from Access-Files.
            Database db = DatabaseBuilder.open(new File(FILE_PATH));

            //List the tables and count progress steps for the progressbar in the GUI.
            System.out.println("Tablenames in the MS-Access file are:" + db.getTableNames());
            int numberProgressSteps = db.getTableNames().size() + 8;
            progress.setSize((long)numberProgressSteps);

            //Does connecting even work with the given credentials?
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            if(mainWindow != null)
            progress.inc(1);

            //Prepare sending statements to mysql.
            stmt = conn.createStatement();

            //Drop all existing Tables with rliste_ as prefix.
            System.out.println("Drop Tables if they already exist in: " + DB_URL);
            String dropStatement = "DROP TABLE IF EXISTS ";
            stmt.execute("SET FOREIGN_KEY_CHECKS=0;");
            for(String sr : db.getTableNames()){
                stmt.executeUpdate(dropStatement + "rliste_" +  sr + ";");
                System.out.println("Dropped Table: " + "rliste_" + sr);
            }
            if(mainWindow != null)
            progress.inc(1);


            //Set up Tableschema in mysql.
            System.out.println("Creating Tables and setting Primarykeys.");
            for(String str : tc.getCreateList()){
                stmt.executeUpdate(str);
            }
            System.out.println("Creating Tables done!");
            if(mainWindow != null)
            progress.inc(1);

            //Parsing and inserting data into mysql, depending on given threadcounts.
            //Use ExecutorService if we have more than one thread available.
            System.out.println("Now filling Tables with data...please wait");
            if(THREADCOUNT > 1){
                System.out.println("Filling with ExecutorService with " + THREADCOUNT + " Threads.");
                ExecutorService es = Executors.newFixedThreadPool(THREADCOUNT);
                for(String sr : db.getTableNames()){
                    Table table = db.getTable(sr);
                    if(mainWindow == null){
                        es.execute(new TableFillWorker(table, DB_URL, USER, PASS));
                    }else
                        es.execute(new TableFillWorker(table, progress,  DB_URL, USER, PASS));
                }
                es.shutdown();
                es.awaitTermination(1, TimeUnit.DAYS);
            }else{
                System.out.println("Filling with Singlethread");
                for(String sr : db.getTableNames()){
                    Table table = db.getTable(sr);
                    ParseData parseData = new ParseData(conn, table);
                    parseData.fillTable();
                    if(mainWindow != null)
                    progress.inc(1);
                }
            }

            //After filling, add foreign keys.
            System.out.println("Setting up foreign keys");
            for(String sr : tc.getForeignList()){
                stmt.executeUpdate(sr);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //After all is finished, give out the measured time to console and GUI Textfield.
        if(mainWindow != null)
        progress.inc(5);
        System.out.println("..parsing to MySQL done. Time: " + (System.currentTimeMillis() - beginningTime) );

        //Make GUI responsive again.

        if(mainWindow != null){
            this.mainWindow.getResultText().setText("Parsing done in " + (System.currentTimeMillis() - beginningTime) + "ms");
            this.mainWindow.getFileButton().setDisable(false);
            this.mainWindow.getStartButton().setDisable(false);
        }

    }


    public Progress getProgress() {
        return progress;
    }


}

