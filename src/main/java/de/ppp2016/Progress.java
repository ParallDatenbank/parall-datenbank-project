package de.ppp2016;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by jjensen on 3/8/16.
 */
public class Progress {
    private final long startTime;
    private long size;
    private StringProperty estimatedTime;
    private DoubleProperty progressProperty;
    private long progress;

    public Progress() {
        this.estimatedTime = new SimpleStringProperty("Parsing running...");
        this.progressProperty = new SimpleDoubleProperty(-1L);
        this.progress = 0;
        this.startTime = System.currentTimeMillis();
    }

    synchronized public void inc(int batchSize) {
        progress += batchSize;
        estimateTime();
        Platform.runLater(() -> progressProperty.setValue((double)progress/(double)size));
    }

    public boolean finished(){
        return size == progress;
    }

    public long time(){
        return System.currentTimeMillis() - this.startTime;
    }

    @Override
    public String toString() {
        return progress + "/" + size + " percent " + ((double)progress/(double)size) + "    Estimated time: " + estimatedTime.getValue();
    }

    private void estimateTime() {
        if (progress!=0) {
            long progress = this.progress;
            double elapsedTime = System.currentTimeMillis() - startTime;
            double progressPerMillisecond = progress / elapsedTime;
            long time = (long) ((size - progress) / progressPerMillisecond);
            long h = time / 3600;
            long min = time / 60000;
            long sec = (time % 60000) / 1000;
            Platform.runLater(() -> estimatedTime.setValue(min + "m" + sec + "s    Elapsed Time: " + elapsedTime));
        }
    }

    public StringProperty getEstimatedTime(){
        return this.estimatedTime;
    }

    public void finish(){
        progress = size;
        progressProperty.setValue(1.0);
        estimatedTime.setValue("Done. Time required: " + time() + "ms");
    }

    public void setSize(long size) {
        this.size = size;
    }

    public DoubleProperty getProgressProperty() {
        return progressProperty;
    }

}
