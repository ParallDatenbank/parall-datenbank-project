package de.ppp2016;

import de.ppp2016.Parser.GelbeListe;
import de.ppp2016.Parser.RoteListe.RotParser;
import de.ppp2016.Parser.GelbeListe2.GelbeListe2;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Created by awalende on 23.02.16.
 */

//TODO: Labels overlapping, progressbar not functioning in gelbeListe, Benchmark not closing automatically
public class MainWindow extends Application {

    private TextField urlTextfield, userTextField, passTextField;
    private NumericTextField threadTextField;
    private ProgressBar progressBar;
    private ComboBox<Database> dropDownMenu;
    private File file;
    private Stage window;
    private Button startButton, fileButton;
    private GridPane layout;

    private Text resultText, sourcePath;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setResizable(false);
        window.setTitle("Projekt Parallele Algorithmen Datenbanken.");

        //Setup Layout
        layout = new GridPane();
        layout.setAlignment(Pos.CENTER);
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setPadding(new Insets(25, 25, 25, 25));

        //Setup Title
        Text scenetitle = new Text("Datenbanken Parser");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20.0D));
        layout.add(scenetitle, 0, 0, 2, 1);

        layout.add(new Text("Database URL:"), 0, 1);
        urlTextfield = new TextField("jdbc:mysql://localhost/");
        layout.add(urlTextfield, 1, 1);

        layout.add(new Text("Number of threads:"), 0, 2);
        threadTextField = new NumericTextField(1);
        layout.add(threadTextField, 1, 2);

        layout.add(new Text("Username:"), 0, 3);
        userTextField = new TextField("test");
        layout.add(userTextField, 1, 3);

        layout.add(new Text("Password:"), 0, 4);
        passTextField = new TextField("test");
        layout.add(passTextField, 1, 4);

        ObservableList<Database> options = FXCollections.observableArrayList(Database.values());
        dropDownMenu = new ComboBox<>(options);
        dropDownMenu.setValue(Database.GELBE);
        layout.add(dropDownMenu, 0, 5);

        //Setup open Button
        fileButton = new Button("Open");
        fileButton.setOnAction(this::openFile);
        layout.add(fileButton, 1, 5);


        //Setup parser Buttons
        startButton = new Button("Start");
        startButton.setDisable(true);
        startButton.setOnAction(this::start);
        layout.add(startButton, 0, 7, 2, 1);


        //Progress Text and Timeresult Text
        layout.add(new Text("Progress:"), 0, 9);
        resultText = new Text("");
        layout.add(resultText, 0, 10);

        //Path to source Database
        sourcePath = new Text();
        layout.add(sourcePath, 0, 6, 2, 1);

        //Add progressBar
        progressBar = new ProgressBar();
        progressBar.setVisible(false);
        layout.add(progressBar, 1, 9, 3, 1);

        //Put together and show.
        Scene scene = new Scene(layout, 600, 600);
        window.setScene(scene);
        window.setOnCloseRequest(e -> System.exit(1));
        window.show();
    }

    private void start(ActionEvent event) {
        switch (dropDownMenu.getValue()) {
            case ROTE:
                try {
                    progressBar.setVisible(true);
                    progressBar.progressProperty().unbind();
                    progressBar.setProgress(-1);
                    progressBar.setMaxWidth(200);

                    resultText.setText("");

                    startButton.setDisable(true);
                    fileButton.setDisable(true);

                    RotParser rotParser = new RotParser(this, file.toPath().toString(),
                            urlTextfield.getCharacters().toString(),
                            userTextField.getCharacters().toString(),
                            passTextField.getCharacters().toString(),
                            threadTextField.getInt());
                    rotParser.start();
                    progressBar.progressProperty().bind(rotParser.getProgress().getProgressProperty());
                } catch (IOException | SQLException e) {
                    e.printStackTrace();
                }
                break;
            case GELBE:
                progressBar.setVisible(true);
                startButton.setDisable(true);
                progressBar.progressProperty().unbind();
                progressBar.setProgress(-1);
                progressBar.maxWidth(200);
                GelbeListe gelbeListe = new GelbeListe(file.toPath()
                        , urlTextfield.getCharacters().toString()
                        , userTextField.getCharacters().toString()
                        , passTextField.getCharacters().toString()
                        , threadTextField.getInt()
                        , startButton);
                gelbeListe.start();
                progressBar.progressProperty().bind(gelbeListe.getProgress().getProgressProperty());
                resultText.textProperty().bind(gelbeListe.getProgress().getEstimatedTime());
                break;
            case GELBE2:
                try {
                    progressBar.setVisible(true);
                    startButton.setDisable(true);
                    progressBar.progressProperty().unbind();
                    progressBar.setProgress(-1);
                    progressBar.maxWidth(200);
                    GelbeListe2 gelbeListe2 = new GelbeListe2(file.toPath()
                            , urlTextfield.getCharacters().toString()
                            , userTextField.getCharacters().toString()
                            , passTextField.getCharacters().toString()
                            , threadTextField.getInt()
                            , startButton);
                    gelbeListe2.start();
                    progressBar.progressProperty().bind(gelbeListe2.getProgress().getProgressProperty());
                    resultText.textProperty().bind(gelbeListe2.getProgress().getEstimatedTime());

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void openFile(ActionEvent event) {
        if (dropDownMenu.getValue() == Database.GELBE || dropDownMenu.getValue() == Database.GELBE2) {
            DirectoryChooser dc = new DirectoryChooser();
            file = dc.showDialog(window);
        } else {
            FileChooser fc = new FileChooser();
            file = fc.showOpenDialog(window);
        }
        if (file != null) {
            System.out.println("Set file location: " + file.getAbsolutePath());
            sourcePath.setText(file.getAbsolutePath());
            startButton.setDisable(false);
        }
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public TextField getUrlTextfield() {
        return urlTextfield;
    }

    public TextField getUserTextField() {
        return userTextField;
    }

    public TextField getPassTextField() {
        return passTextField;
    }

    public NumericTextField getThreadTextField() {
        return threadTextField;
    }

    public void setResultText(Text resultText) {
        this.resultText = resultText;
    }

    public Text getResultText() {
        return resultText;
    }

    public ComboBox<Database> getDropDownMenu() {
        return dropDownMenu;
    }

    public Stage getWindow() {
        return window;
    }

    public Button getStartButton() {
        return startButton;
    }

    public Button getFileButton() {
        return fileButton;
    }

    public GridPane getLayout() {
        return layout;
    }

    /**
     * Main method
     * @param args use '-nogui [path to config file]' to start without gui.
     *             Default value for config file is 'database.config'
     */
    public static void main(String[] args) {
        boolean nogui = false;
        Path configFile = new File("database.config").toPath();
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-nogui")) nogui = true;
            if (args.length > i+1) configFile = new File(args[i+1]).toPath();
        }
        if (Arrays.stream(args).anyMatch(s -> s.equals("-nogui"))) {
            System.out.println("Starting without gui");
            System.out.println("Loading config from file database.config");
            try {
                //load config file to map
                Map<String, String> config = Files.lines(new File("database.config").toPath())
                        .map(s -> s.split(" "))
                        .filter(array -> array.length == 2)
                        .collect(Collectors.toMap(array -> array[0], array -> array[1]));
                if (!checkConfig(config)) return;
                Database db = Database.valueOf(config.get("db"));

                String s;
                s = config.get("thread_num");
                Integer numThreads;
                if (s != null) numThreads = Integer.parseInt(s);
                else numThreads = 1;
                s = config.get("db_path");
                Path dbpath = Paths.get(s);
                String dbUrl, dbuser, dbpassword;
                dbUrl = config.get("db_url");
                dbuser = config.get("user");
                dbpassword = config.get("password");
                switch (db){
                    case GELBE:
                        new GelbeListe(dbpath, dbUrl, dbuser, dbpassword, numThreads, null).run();
                        break;
                    case GELBE2:
                        try {
                            new GelbeListe2(dbpath, dbUrl, dbuser, dbpassword, numThreads, null).run();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        break;
                    case ROTE:
                        try {
                            new RotParser(null, dbpath.toString(), dbUrl, dbuser, dbpassword, numThreads).run();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            launch(args);
        }
    }

    private static boolean checkConfig(Map<String, String> config) {
        if (config.get("db") == null) {
            System.err.println("Database not specified");
            return false;
        }
        if (config.get("db_url") == null) {
            System.err.println("Database URL not specified");
            return false;
        }
        return true;
    }


    public File getFile() {
        return file;
    }

    /**
     * A TextField only accepting Integers
     */
    public class NumericTextField extends TextField {

        //Sag mal ist das jetzt die neue Hipstermethoden um Sachen in String zu casten?
        //Aber Klarinette
        NumericTextField(int i) {
            super(i + "");
        }

        @Override
        public void replaceText(IndexRange range, String text) {
            if (validate(text)) super.replaceText(range, text);
        }

        @Override
        public void replaceText(int start, int end, String text) {
            if (validate(text)) super.replaceText(start, end, text);
        }

        @Override
        public void replaceSelection(String replacement) {
            if (validate(replacement)) super.replaceSelection(replacement);
        }

        private boolean validate(String text) {
            return text.matches("[0-9]*");
        }

        public int getInt() {
            return Integer.parseInt(this.getCharacters().toString());
        }
    }
}

/**
 * Enum to represent the databases
 */
enum Database {
    GELBE("Gelbe Liste"), GELBE2("Gelbe Liste 2"), ROTE("Rote Liste");

    private String name;

    Database(String s) {
        this.name = s;
    }

    @Override
    public String toString() {
        return name;
    }
}