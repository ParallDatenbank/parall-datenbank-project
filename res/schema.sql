CREATE TABLE `rliste_Abgabebestimmungen`
 (
 `NameKurz`   varchar (20) NOT NULL,
 `Name`   varchar (100),
 PRIMARY KEY(NameKurz)
);

CREATE TABLE `rliste_Abgabestimmungen_AbgGruppe`
 (
 `idxAbgGruppen`   int NOT NULL,
 `NameKurzAbgabebestimmung`   varchar (20) NOT NULL,
 PRIMARY KEY(idxAbgGruppen, NameKurzAbgabebestimmung)
);

CREATE TABLE `rliste_AbgGruppen`
 (
 `idx`   int NOT NULL,
 `Name`   varchar (100),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Chiffren`
 (
 `Signatur`   varchar (20),
 `Daten`   TEXT,
 `Verwendung`   varchar (4),
 PRIMARY KEY(Signatur)
);

CREATE TABLE `rliste_Darreichungsformen`
 (
 `idx`   int NOT NULL,
 `idxPraeparate`   int NOT NULL,
 `Sortierung`   int,
 `Name`   varchar (510),
 `NameRtf`   varchar (510),
 `Sortname`   varchar (510),
 `EinnahmeformKurz`   varchar (6),
 `Abgabebestimmung`   varchar (20),
 `Kohlenhydrate`   varchar (510),
 `Eigenbestand`   varchar(10),
 `PBSCode`   varchar (100),
 `Fachinfo`   varchar(10),
 `Mono`   varchar (10),
 `Medizinproduktkennz`   varchar (100),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Darreichungsformen_ATC`
 (
 `idx`   int,
 `idxDarreichungsformen`   int,
 `ATC`   varchar (20),
 `Sortierung`   int,
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Darreichungsformen_ENummer`
 (
 `idxDarreichungsformen`   int NOT NULL,
 `idxENummer`   int NOT NULL,
 `Stoffcode`   varchar (20),
 PRIMARY KEY(idxDarreichungsformen, idxENummer)
);

CREATE TABLE `rliste_Darreichungsformen_Hilfsstoffe`
 (
 `idxDarreichungsformen`   int NOT NULL,
 `CodeHilfsstoffe`   varchar (20) NOT NULL,
 PRIMARY KEY(idxDarreichungsformen, CodeHilfsstoffe)
);

CREATE TABLE `rliste_Einnahmeformen`
 (
 `NameKurz`   varchar (6),
 `Name`   varchar (100)
);

CREATE TABLE `rliste_ENummer`
 (
 `idx`   int,
 `ENummer`   varchar (260),
 `ENummersort`   varchar (100),
 `EName`   varchar (500),
 `ENummerSuch`   varchar (100),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_ENummerStoffe`
 (
 `idxENummer`   int,
 `Stoffcode`   varchar (20),
 `Stoffbez`   varchar (260),
 `Zeile`   int,
 `Name98`   varchar (260),
 PRIMARY KEY(idxENummer, Stoffcode)
);

CREATE TABLE `rliste_Firmen`
 (
 `idx`   int NOT NULL,
 `NameKurz`   varchar (510),
 `Name`   varchar (510),
 `Sortname`   varchar (510),
 `Strasse`   varchar (510),
 `Plz`   varchar (20),
 `Ort`   varchar (510),
 `Postfach`   varchar (100),
 `PlzPostfach`   varchar (100),
 `OrtPostfach`   varchar (100),
 `Vorwahl`   varchar (30),
 `Telefon`   varchar (510),
 `Telefax`   varchar (510),
 `eMail`   varchar (510),
 `WWW`   varchar (510),
 `Sonstiges`   TEXT,
 `NotTelefon`   TEXT,
 `Verweis`   varchar (510),
 `VerweisIdx`   int,
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Firmen_Fast`
 (
 `Idx`   int,
 `NameKurz`   varchar (510),
 `Sortname`   varchar (510),
 `Zeile`   int,
 PRIMARY KEY(Idx)
);

CREATE TABLE `rliste_FirmenVerweise`
 (
 `idxFirma`   int NOT NULL,
 `idxFirmaVerweis`   int NOT NULL,
 PRIMARY KEY(idxFirma, idxFirmaVerweis)
);

CREATE TABLE `rliste_Gliederung`
 (
 `idx`   int NOT NULL,
 `Gliederung`   varchar (100),
 `Name`   varchar (510),
 `idxVerweis`   int,
 `Hierarchie`   int,
 `SortGliederung`   varchar (100),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Gliederung_Praeparate`
 (
 `idxGliederung`   int NOT NULL,
 `idxPraeparate`   int NOT NULL,
 `Haupteintrag`   varchar(10),
 PRIMARY KEY(idxGliederung, idxPraeparate)
);

CREATE TABLE `rliste_gnwEintraege`
 (
 `IdxGnw`   int,
 `Typ`   varchar (10),
 `Sortierung`   int,
 `Punkt`   varchar (20),
 `Daten1`   TEXT,
 `Daten2`   TEXT,
 PRIMARY KEY(IdxGnw, Typ, Sortierung)
);

CREATE TABLE `rliste_GnwTypen`
 (
 `NameKurz`   varchar (10),
 `Name`   varchar (100),
 `Sortierung`   varchar (10),
 PRIMARY KEY(NameKurz)
);

CREATE TABLE `rliste_Haupt_Fast`
 (
 `Idx`   int,
 `Gliederung`   varchar (100),
 `Name`   varchar (510),
 `Zeile`   int
);

CREATE TABLE `rliste_Hilf_Fast`
 (
 `Code`   varchar (20),
 `Name`   varchar (260),
 `SortName`   varchar (260),
 `Zeile`   int,
 `Name98`   varchar (260)
);

CREATE TABLE `rliste_Hilfsstoffe`
 (
 `Code`   varchar (20) NOT NULL,
 `Name`   varchar (460) NOT NULL,
 `Sortname`   varchar (260),
 `Synonym`   varchar(10),
 `Name98`   varchar (460),
 PRIMARY KEY(Code, Name)
);

CREATE TABLE `rliste_Packungen`
 (
 `idx`   int NOT NULL,
 `idxDarreichungsformen`   int NOT NULL,
 `Pzn`   varchar (16),
 `Handelsname_inkl_Packungsangaben`   varchar (510),
 `Packungsangaben`   varchar (510),
 `AnstaltsPck`   varchar(10),
 `Festbetrag`   varchar (40),
 `ApothekenVK`   varchar (40),
 `Einnahmeform`   varchar (6),
 `NameGeneriert`   varchar(10),
 `Eigenbestand`   varchar(10),
 `Benutzerpackung`   varchar(10),
 `Quelle`   varchar (100),
 `nav`   varchar (2),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_pharmako`
 (
 `CodeStoffe`   varchar (18),
 `StoffGruppe`   varchar (510),
 `Pharmako`   TEXT,
 `Hwz`   varchar (400),
 `Hd`   varchar (400),
 `Hp`   varchar (80),
 `Signatur`   varchar (150),
 `Typ`   varchar (20),
 PRIMARY KEY(CodeStoffe)
);

CREATE TABLE `rliste_Praep_Fast`
 (
 `Idx`   float,
 `Name`   varchar (510),
 `Fachinfo`   varchar(10),
 `MonoPraep`   int,
 `Zeile`   int,
 `SortName`   varchar (510),
 `Eigenbestand`   varchar(10)
);

CREATE TABLE `rliste_Praeparate`
 (
 `idx`   int NOT NULL,
 `Name`   TEXT,
 `NameRtf`   TEXT,
 `Sortname`   varchar (510),
 `Fachinfo`   varchar(10),
 `MonoPraep`   int,
 `Anwendung`   TEXT,
 `Gegenanzeige`   TEXT,
 `AnwBeschraenkung`   TEXT,
 `Nebenwirkungen`   TEXT,
 `Wechselwirkungen`   TEXT,
 `Intoxikation`   TEXT,
 `Schwangerschaft`   TEXT,
 `Stillzeit`   TEXT,
 `SchwStill`   TEXT,
 `Warnhinweis`   TEXT,
 `Dosierung`   TEXT,
 `Hinweis`   TEXT,
 `Lagerungshinweis`   varchar (510),
 `Anstaltspackung`   TEXT,
 `Klinikpackung`   TEXT,
 `AnzDarreichungsformen`   int,
 `Eigenbestand`   varchar(10),
 `PraepNum`   varchar (510),
 `HGL`   varchar (510),
 `ZusUeberwachung`   varchar(10),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Praeparate_Firma`
 (
 `idxPraeparate`   int NOT NULL,
 `idxFirma`   int NOT NULL,
 `Typ`   varchar (2),
 `Sortierung`   int,
 PRIMARY KEY(idxPraeparate, idxFirma)
);

CREATE TABLE `rliste_PZN_Fast`
 (
 `idxDarreichungsform`   int NOT NULL,
 `pzn`   varchar (16) NOT NULL,
 `Zeile`   int,
 `Eigenbestand`   varchar(10),
 `Name`   varchar (510)
);

CREATE TABLE `rliste_Stich_Fast`
 (
 `Idx`   int,
 `Stichwort`   varchar (510),
 `Gruppe`   varchar (510),
 `Begriff`   varchar(10),
 `Zeile`   int
);

CREATE TABLE `rliste_Stichwortverzeichnis`
 (
 `Idx`   int,
 `Stichwort`   varchar (510),
 `Gruppe`   varchar (510),
 `Begriff`   varchar(10),
 PRIMARY KEY(Idx)
);

CREATE TABLE `rliste_Stoffe`
 (
 `Code`   varchar (20),
 `Name`   varchar (260),
 `Synonym`   varchar(10),
 `NameSort`   varchar (260),
 `Name98`   varchar (260)
);

CREATE TABLE `rliste_Stoffsynonyme`
 (
 `code`   varchar (18),
 `Name`   varchar (260),
 `NameRTF`   TEXT,
 `Synonym`   varchar (510),
 `SynonymRTF`   varchar (510),
 `SortName`   varchar (260)
);

CREATE TABLE `rliste_Version`
 (
 `idx`   int NOT NULL,
 `Version`   int,
 `Beschreibung`   varchar (100),
 PRIMARY KEY(idx)
);

CREATE TABLE `rliste_Wirk_Fast`
 (
 `Code`   varchar (20),
 `Name`   varchar (260),
 `SortName`   varchar (260),
 `Zeile`   int,
 `Medizinprodukt`   varchar(10),
 `Name98`   varchar (260)
);

CREATE TABLE `rliste_Wirkstoffe`
 (
 `Code`   varchar (20) NOT NULL,
 `Name`   varchar (460) NOT NULL,
 `Sortname`   varchar (460),
 `Synonym`   varchar(10),
 `Medizinprodukt`   varchar(10),
 `Name98`   varchar (460),
 PRIMARY KEY(Code, Name)
);

CREATE TABLE `rliste_Darreichungsformen_Wirkstoffe`
 (
 `idxDarreichungsformen`   int NOT NULL,
 `CodeWirkstoffe`   varchar (20) NOT NULL,
 `Anzeigen`   varchar(10),
 `Medizinprodukt`   varchar(10),
 PRIMARY KEY(idxDarreichungsformen, CodeWirkstoffe)
);

CREATE TABLE `rliste_gnw`
 (
 `Idx`   int,
 `Signatur`   varchar (10) NOT NULL,
 `SubSignatur`   varchar (20),
 `Name`   TEXT NOT NULL,
 `SubName`   varchar (510),
 `Monographie`   TEXT,
 `IdxParent`   int,
 `Hierarchie`   int NOT NULL,
 PRIMARY KEY(Idx)
);

CREATE TABLE `rliste_Komponenten`
 (
 `idx`   int NOT NULL,
 `idxDarreichungsformen`   int NOT NULL,
 `Wirkstoffe`   TEXT,
 `Hilfsstoffe`   TEXT,
 PRIMARY KEY(idx)
);

